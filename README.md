# This Thing That is Come to Pass!

The idea for this piece came to me in September 2011 while I was sat on a bus to
the University of York campus for the final UK e-Science All Hands Meeting. The
titular part of the chorus sort of popped into my head fully formed.

Actually getting the thing down in notation, and in particular coming up with a
suitable verse, proved rather trickier, and so it has taken many hours of work
and years of procrastination for me to get it finally in a form where it might,
just might, be performed.

In that original flash of insight, if such it was, I had it in mind for the
piece to be sung by a children's choir. The ATB parts are there to give it a bit
of depth and additional interest.

It is supposed to be fun and energetic, with a sense of forward movement and
excitement. I'm afraid the piano part may require a certain level of endurance.

## Summary information

  - *Voicing:* SATB

  - *Notes on ambitus:* SA should be comfortably in the range of most singers.
    The Tenor has a top E, and the Bass has bottom Fs.

  - *Instrumentation:* Piano

  - *Approximate performance length:* 3:36

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Tags
page](https://gitlab.com/alex-ball-music/this-thing/tags).
