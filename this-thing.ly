\version "2.24.0"
\language "english"
%theStaffSize = 19
\include "ajb-common.ly"
\include "articulate.ly"

#(define choral-instrument-equalizer-alist '())

#(set! choral-instrument-equalizer-alist
  (append
    '(
      ("choir aahs" . (0.3 . 1.0))
      ("church organ" . (0.1 . 0.8)))
    choral-instrument-equalizer-alist))

#(define (choral-instrument-equalizer s)
  (let ((entry (assoc s choral-instrument-equalizer-alist)))
    (if entry
      (cdr entry))))

\header {
  dedication = "For Viv Wilkinson"
  title = "This Thing That Is Come to Pass"
  composer = "Alex Ball"
  copyright = \markup { %\override #'(baseline-skip . 2.5)
  \center-column {
    "© Alex Ball 2020. This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Licence:"
    "https://creativecommons.org/licenses/by-nc/4.0/"
  } }
  % Remove default LilyPond tagline
  tagline = "v3 – 2025-01-05"
}

\paper {
  system-system-spacing =
    #'((basic-distance . 16)
     (minimum-distance . 12)
     (padding . 1)
     (stretchability . 60))
  left-margin = 10
  right-margin = 10
}

choirControl = {
  s1*2
  \repeat volta 2 {
    s1*2
    s1*16
    \bar "||" \mark\default
    s1*24
  }
  s1*2
  \bar "||" \mark\default
  s1*16
  \bar "||"\mark\default
  s1*26
  \bar "|."
}

global = {
  \key c \major
  \time 2/2
  \tempo "Cheerfully" 4 = 144
}

%%%%

verseOneLyrics = \lyricmode {
  \set stanza = \markup{ \medium { " S.A." } "1." }
  Shep -- herds in fields a -- \reSlur bi -- ding,
  Keep -- ing watch by night,
  The an -- gel of the Lord ap -- peared,
  The dark -- ness turned to light.
  ‘Make haste,’ he said, ‘to Beth -- le -- hem,
  To greet the new -- born King!’
  The shep -- herds start -- ed run -- ning,
  each be -- gan to sing:
}

verseTwoLineOneLyricsLayout = \lyricmode {
  \set stanza = \markup{ \medium { " T.B." } "2." }
  \set associatedVoice = "v2"
  East -- ern ma -- \set associatedVoice = "soprano" gi, _ tra -- cing
}

verseTwoLineOneLyrics = \lyricmode {
  \set stanza = "2."
  East -- ern ma -- gi, _ tra -- cing
}

verseTwoLineTwoPlusLyrics = \lyricmode {
  Move -- ment in the skies,
  Dis -- co -- vered signs a -- mong the stars,
  That bright -- ened up their eyes.
}

verseTwoLineFiveLyricsLayout = \lyricmode {
  They knew they had to fol -- low \unSlur the star, \reSlur
}

verseTwoLineFiveLyrics = \lyricmode {
  They knew they had to fol -- low the star,
}

verseTwoLineSixPlusLyrics = \lyricmode {
  They worked out what it meant,
  They hur -- ried off to Ju -- dah,
  sing -- ing as they went:
}

verseTwoLyricsLayout = \lyricmode {
  \verseTwoLineOneLyricsLayout
  \verseTwoLineTwoPlusLyrics
  \verseTwoLineFiveLyricsLayout
  \verseTwoLineSixPlusLyrics
}

verseTwoLyrics = \lyricmode {
  \verseTwoLineOneLyrics
  \verseTwoLineTwoPlusLyrics
  \verseTwoLineFiveLyrics
  \verseTwoLineSixPlusLyrics
}

verseThreeSopranoLyrics = \lyricmode {
  \set stanza = "3."
  Here we are at Christ -- mas,
  Do -- ing what we can
  To ce -- le -- brate that bles -- sed time
  When God be -- came a man.
  We may have missed things first time round
  But in our hearts we're there:
  We’ve heard a -- bout those won -- ders,
  Let’s show that we care:
}

chorusALyrics = \lyricmode {
  Let us go see this thing that is come to pass! __
  Let us go see this thing that is come to pass! __
  As the Lord a -- bove is our wit -- ness,
  We’ll be a wit -- ness to our Lord on earth.
  We’ll praise him till the end of time,
  Let’s praise him at his birth!
  Let us go see this thing that is come to pass! __
  Let us go see this thing that is come to pass! __
}

chorusBLyrics = \lyricmode {
  Let us go see this thing that is come to pass! __
  Let us go see this thing that is come to pass! __
  As the Lord a -- bove is our wit -- ness,
  We’ll be a wit -- ness to our Lord on earth. __
  We’ll praise him till the end of time,
  Let’s praise him at his birth! __
  So let us go see this thing that is come to pass! __
  Let us go see this thing that is come to pass! __
}

verseThreeBassLyrics = \lyricmode {
  \set stanza = "3."
  Here we are at Christ -- mas,
  Do -- ing what we can
  To ce -- le -- brate that bles -- sed time
  When God be -- came a man.
  We may have missed things first time round
  But in our hearts we're there:
  We’ve heard a -- bout those won -- ders,
  Let’s show that we care:
}

%%%%

verseOneAndTwoLineOneVoice = \relative c' {
  << { \voiceOne c8 c c4 c4 c8 } \new Voice = "v2" { \voiceTwo \magnifyMusic 0.63 { c4 c8 c8 ~ c8 c4 } } >> \oneVoice e8 ~
}

verseOneLineOneVoice = \relative c' {
  c8 c c4 c4 c8 e8 ~
}

verseTwoLineOneVoice = \relative c' {
  c4 c8 c8 ~ c8 c4 e8 ~
}

verseOneAndTwoLineTwoPlusVoice = \relative c' {
  e8 c ~ c2 r4
  f4 f8 a ~ a g4 f8 ~
  f2. r8 e
  d4 d8 d ~ d e4 fs8 ~
  fs8 g4 a8 ~ a4 r8 d,
  g4 g8 b ~ b a4 g8 ~
  g2 r4 g4
}

verseOneAndTwoLineFiveVoice = \relative c'' {
  a4 g8 f ~ f a4 g8 ~
  g8 f4 \slurDashed e8( e4) \slurSolid r8 g8
}

verseOneLineFiveVoice = \relative c'' {
  a4 g8 f ~ f a4 g8 ~
  g8 f4 e8 ~ e4 r8 g8
}

verseTwoLineFiveVoice = \relative c'' {
  a4 g8 f ~ f a4 g8 ~
  g8 f4 e8 e4 r8 g8
}

verseOneAndTwoLineSixPlusVoice = \relative c'' {
  gs4 e8 b' ~ b c4 a8 ~
  a2 r4. e8
  d4 e8 fs ~ fs g4 a8( ~
  a8 b) a2 r4
  g4 g8 g ~ g a4 b8 ~
  b2. r4
}

verseOneAndTwoVoice = {
  \verseOneAndTwoLineOneVoice
  \verseOneAndTwoLineTwoPlusVoice
  \verseOneAndTwoLineFiveVoice
  \verseOneAndTwoLineSixPlusVoice
}

chorusASopranoVoice = \relative c'' {
  a8 a f4 a f8 a ~
  a a4 a8 c4 a
  c1 ~
  c2 r2
  c8 c a4 c a8 c ~
  c c4 a8 c4 a
  b2( ~ b8 bf a g ~
  g2) r4 g8 gs
  a4. f8 c' b a g ~
  g8 g8 ~ g4 r8 fs8 g gs
  a8 a f a c( b) a g ~
  g2. r8 gs8
  a4 f c'8( b) a g ~
  g8 gs4 a8 ~ a4 r8 e8
  d4 e fs8( g) a b ~
  b2. r4
  c8 c a4 c a8 c ~
  c c4 a8 c4 a
  c1 ~
  c2 r2
  c8 c a4 c a8 c ~
  c c4 c8 ef4 d
  c1 ~
  c2 r2
}

verseThreeSopranoVoice = \relative c' {
  c4^\markup \smcp {soprano + tenor} c4 c4 c8 e8 ~
  e8 c ~ c2 r4
  f4 f8 a ~ a g4 f8 ~
  f2. r8 ef
  d4 d8 d ~ d e4 fs8 ~
  fs8 g4 a8 ~ a4 d,
  g4 g8 b ~ b a4 g8 ~
  g2 r4 g4
  a4 g8 f ~ f a4 g8 ~
  g8 f4 e8 ~ e4 r8 g8
  gs4 e8 b' ~ b c4 a8 ~
  a2 r4. e8
  d4 e8 fs ~ fs g4 a8( ~
  a8 b) a2 r4
  g4 g8 g ~ g a4 b8 ~
  b2. r4
}

chorusBSopranoVoice = \relative c'' {
  a8 a f4 a f8 a ~
  a a4 a8 c4 a
  c1 ~
  c2 r2
  c8 c a4 c a8 c ~
  c c4 a8 c4 a
  b2( ~ b8 c cs d ~
  d2) r4 d8 b
  c4. c8
    << { \voiceOne f g f e ~ } \new Voice { \voiceTwo \magnifyMusic 0.63  { c d f e } } >>
    \oneVoice
  e8 e8 ~ e4 r8 d8 c b
  c8 c a c e4
    << { \voiceOne d8 f( ~ f4. e8 ~ e4) } \new Voice { \voiceTwo \magnifyMusic 0.63 { d8 c ~ c2. } } >>
    \oneVoice r8 b8
  c4 d c8( b) a c ~
  c8 d4 c8 ~ c4 r8 cs8
  d4 d c8( b) a b( ~
  b8 c4 d8) r4 b4
  c8 c a4 c a8 c ~
  c c4 a8 c4 a
  c1 ~
  c2 r2
  c8 c a4 c a8 c ~
  c c4 c8 ef8( c) ef( e)
  c1 ~
  c2 r2
  R1
  R1
}

sopranoVoiceLayout = {
  \global
  \dynamicUp
  R1*2
  % Verse 1/2
  \repeat volta 2 {
    R1*2
    \verseOneAndTwoVoice
    \splitStaffBarLine
    \set Staff.explicitClefVisibility = #end-of-line-invisible
    \clef treble
    \set instrumentName = \markup \center-column { "S." "A." }
    % Chorus
    \voiceOne
    \chorusASopranoVoice
  }
  R1*2
  % Last verse
  \unset Staff.explicitClefVisibility
  \clef "treble_(8)"
  \set Staff.middleCPosition = -6
  \oneVoice
  \verseThreeSopranoVoice
  \chorusBSopranoVoice
}

chorusAAltoVoice = \relative c' {
  c8 c c4 c c8 c8 ~
  c8 c4 c8 ef4 d
  c1 ~
  c2 r2
  c8 c c4 c c8 d8 ~
  d8 d4 d8 d4 d
  b1 ~
  b2 r4 e8 d
  c4. c8 f g f e ~
  e8 e ~ e4 r8 e e d
  c8 c c d f4 f8 f( ~
  f4. e8 ~ e4) r8 e
  f8( e) d4 d8( e8) f8 e8 ~
  e8 e4 e8 ~ e4 r8 e8
  d4 d4 d8( e) fs g ~
  g2. r4
  f8 f f4 f f8 f8 ~
  f8 f4 f8 f4 f
  e1 ~
  e2 r2
  f8 f f4 f f8 f8 ~
  f8 f4 f8 a4 a
  g1 ~
  g2 r2
}

verseThreeAltoVoice = \relative c' {
  R1^\markup \smcp {alto + bass}
  r4. c8 ~ c d e8 c
  f4. f8 ~ f4 r4
  r8 f4 f8 e e ef ef
  d4 d8  d8 ~ d8 d4 d8 ~
  d8 e4 fs8 ~ fs4 d4
  b4 b8 d8~ d8 c4 b8 ~
  b2 r4 d4
  f4 e8 d8 ~ d8 f4 e8 ~
  e8 d4 c8 ~ c4 r8 d8
  e4 e8 e ~ e d4 c8 ~
  c2 r4. c8
  d4 d8 d ~ d e4 fs8 ~
  fs4 fs2 r4
  g4 g8 f ~ f e4 d8~
  d2. r4
}

chorusBAltoVoice = \relative c' {
  a'8 a f4 a f8 a ~
  a a4 a8 c4 a
  c1 ~
  c2 r2
  c8 c a4 c a8 c ~
  c c4 a8 c4 a
  b2( ~ b8 bf a g ~
  g2) r4 g8 gs
  a4. f8 c' b a g ~
  g8 g8 ~ g4 r8 fs8 g gs
  a8 a f a c( b) a g ~
  g2. r8 gs8
  a4 f c'8( b) a g ~
  g8 gs4 a8 ~ a4 r8 e8
  d4 e fs8( g) a b ~
  b2 r4 b4
  a8 a f4 a f8 a ~
  a a4 a8 c4 a
  c1 ~
  c2 r2
  a8 a f4 a f8 a ~
  a a4 a8 c4 a
  c1 ~
  c2 r2
  R1
  R1
}

altoVoiceLayout = \relative c' {
  \global
  \dynamicUp
  R1*2
  % Verse 1/2
  \repeat volta 2 {
    R1*2
    s1*16
    \voiceTwo
    \chorusAAltoVoice
  }
  R1*2
}

chorusATenorVoice = \relative c' {
  a8 a a4 a a8 a8 ~
  a8 a4 a8 a4 a
  g1 ~
  g2 r2
  a8 a a4 a a8 a8 ~
  a8 a4 a8 a4 a
  d1 ~
  d2 r4 b8 b
  c4. a8 a a b c ~
  c8 bf ~ bf4 r8 bf8 bf b
  c8 f, a b c8( a) b8 c8 ~
  c2. r8 b8
  a4 a f c'8 c8~
  c d4 c8 ~ c4 r8 a8
  fs4 a a8( b) c b( ~
  b8 c4 d8 c4) r4
  a8 a a4 a f8 a8 ~
  a8 a4 f8 a4 c
  g1 ~
  g2 r2
  a8 a a4 a f8 a8 ~
  a8 a4 f8 a4 c
  e1 ~
  e2 r2
}

tenorVoiceLayout = {
  \global
  \dynamicUp
  R1*2
  % Verse 1/2
  \repeat volta 2 {
    R1*2
    R1*16
    \voiceOne
    \chorusATenorVoice
  }
  R1*2
}

chorusABassVoice = \relative c, {
  f8 f f4 f f8 f8 ~
  f8 f4 f8 a4 b
  c1 ~
  c2 r2
  f8 f f4 f e8 d8 ~
  d8 d4 d8 f4 fs
  g2( ~ g8 d4 g,8) ~
  g2 r4 g8 e
  f4. f8 a g f c'8 ~
  c8 c8 ~ c4 r8 a8 g e
  f8 f a f a( g) f c' ~
  c2. r8 e8
  f4 f, a f8 c'8 ~
  c8 b4 a8 ~ a4 r8 cs8
  d4 c b a8 g ~
  g2. r4
  f8 f f4 f f8 f8 ~
  f8 a4 f8 a4 f
  c'1 ~
  c2 r2
  f,8 f f4 f f8 f8 ~
  f8 a4 f8 a4 f
  c'1 ~
  c2 r2
}

bassVoiceLayout = {
  \global
  \dynamicUp
  R1*2
  % Verse 1/2
  \repeat volta 2 {
    R1*2
    R1*16
    \voiceTwo
    \chorusABassVoice
  }
  R1*2
  % Last verse
  \oneVoice
  \new Voice = "bassVerse" {
    \clef "treble_(8)"
    \set Staff.middleCPosition = -6
    \verseThreeAltoVoice
  }
  \chorusBAltoVoice
}

%%%%

choirPartLayout = \new ChoirStaff <<
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = "Voices"
  } <<
    \clef "treble_(8)"
    \set Staff.middleCPosition = -6
    \new Voice = "soprano" { \voiceOne \sopranoVoiceLayout }
    \new Voice = "alto" { \voiceTwo \altoVoiceLayout }
  >>
  \new Lyrics \lyricsto "soprano" { \verseOneLyrics \chorusALyrics \verseThreeSopranoLyrics \chorusBLyrics }
  \new Lyrics \lyricsto "soprano" { \verseTwoLyricsLayout }
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenorVoiceLayout }
    \new Voice = "bass" { \voiceTwo \bassVoiceLayout }
  >>
  \new Lyrics \lyricsto "bassVerse" \verseThreeBassLyrics
>>

%%%%

rightOne = \relative c' {
  \global
  % Intro
  e4 e f8 e d e ~
  e8 f4 g8 f4 c8 d
  \repeat volta 2 {
    e4 e <d b>8 <c a> <b g> <c g e> ~
    <c g e>8 <c g e>4 <c g e>8 <c g e>2
    % Verse 1
    c4 c c c8 e ~
    e8 c4 e8 ~ e g4.
    f4 f8 a ~ a g4 f8 ~
    f4. f8 f f f e
    d4 d8 d ~ d e4 fs8 ~
    fs8 g4 a8 ~ a8 d, d d
    g4 g8 b ~ b a4 g8 ~
    g8 a4 g8 a4 g4
    <a f>4 g8 f ~ f <a f>4 <g e>8 ~
    q8 f4 e8 ~ e8 e e g
    gs4 e8 b' ~ b c4 a8 ~
    a4. a4. <a e c>4
    d,4 e8 fs ~ fs g4 a8( ~
    a8 b) a4 a b8 a
    g4 g8 g ~ g a4 b8 ~
    b8 c4 d8 c4 b4
    % Chorus
    \oneVoice
    <a f c>2 q4. q8 ~
    q8 q4 q8 <c a f ef>4 <a f d>
    <c g>4 q q q8 q ~
    q8 q4 q8 q4 q4
    <c a f>2 q4. q8 ~
    q8 q4 <a f d>8 <c a f>4 <c a fs d>
    <b g d>8 q8 q4 q4 <g d b>8 <b g d>8 ~
    <b g d>8 <a g d>4 <g d b>8 <g d b>4. r8
    <a f c>2 q4. <g e c>8 ~
    q8 <g e bf>4 q8 \voiceOne g8 fs \oneVoice <g e bf> <gs d b>
    <a f c>2 q4. <g f c>8 ~
    q4. <g e c>8 q q q <gs e b>
    <a f c>4 <a f d> <a f c> <a f c>8 <g e c>8 ~
    q8 <gs e d>4 <a e c>8 ~ q4 <a e cs>
    <d, a fs>4 <e d a> <fs d a>8( <g e b>) <a fs c> <b g d>8 ~
    q8 <c g d>4 <d b>8 <c g d>4 <b g d>4
    <c a f>2 q4. q8 ~
    q8 q4 q8 <c a f ef>4 <a f d>
    \voiceOne <c g>4 q q q8 <c g> ~
    q8 q4 q8 q4 q \oneVoice
    <c a f>2 q4. q8 ~
    q8 q4 q8 <ef c a>4 <d c a f>
    \voiceOne <c g>4 q q q8 q ~
    q8 q4 q8 q4 <g e>4
  }
  e4 e <d b>8 <c a> <b g> <c g e> ~
  <c g e>8 <c g e>4 <c g e>8 <c g e>2
  % Last verse
  c4 c c c8 e ~
  e8 c4 e8 ~ e g4.
  f4 f8 a ~ a g4 f8 ~
  f4. f8 e e ef ef
  d4 d8 d ~ d e4 fs8 ~
  fs8 g4 a8 ~ a8 d, d d
  g4 g8 b ~ b a4 g8 ~
  g8 a4 g8 a4 g4
  <a f>4 g8 f ~ f <a f>4 <g e>8 ~
  q8 f4 e8 ~ e8 e e g
  gs4 e8 b' ~ b c4 a8 ~
  a4. a4. <a e c>4
  d,4 e8 fs ~ fs g4 a8( ~
  a8 b) a4 a b8 a
  g4 g8 g ~ g a4 b8 ~
  b8 c4 d8 c4 b4
  % Coda
  \oneVoice
  <a f c>2 q4. q8 ~
  q8 q4 q8 <c a f ef>4 <a f d>
  <c g>4 q q q8 q ~
  q8 q4 q8 q4 q4
  <c a f>2 q4. q8 ~
  q8 q4 <a f d>8 <c a f>4 <c a fs d>
  <b g d>8 q8 q4 q4 <g d b>8 <b g d>8 ~
  <b g d>8 <c g d>4 <d b g>8 q4. r8
  <c a f>2 <f c a>4. <e c g>8 ~
  q8 4 <bf g e>8 \voiceOne <bf g>8 <d fs,> \oneVoice <c g e> <b gs d>
  <c a f>2 <e c a f>4. <f c g>8 ~
  q4. <e c g>8 ~ q q <d b g>8 <b gs e>
  <c a f>4 <d a f> <c a f> q8 <c g e>8 ~
  q8 <d gs, e>4 <c a e>8 ~ q4 <cs a e>
  <d a fs>4 <d a e> <d a fs>4 <c a fs>8 <b g d>8 ~
  q8 <c g d>4 <d b>8 <c g d>4 <b g d>4
  <c a f>2 q4. q8 ~
  q8 q4 q8 <c a f ef>4 <a f d>
  \voiceOne <c g>4 q q q8 <c g> ~
  q8 q4 q8 q4 q \oneVoice
  <c a f>2 q4. q8 ~
  q8 q4 q8 <ef c a>4  << { ef8( e) } \\ { <c a>4 } >>
  \voiceOne
  e4 e f8 e d e ~
  e8 f4 g8 f4 c8 d
  e4 e <d b>8 <c a> <b g> <c g e> ~
  <c g e>8 <c c'>4 <c c'>8 <c c'>4 r4
}

rightTwo = \relative c' {
  \global\voiceTwo
  % Intro
  <c g>4 <c g> c <c g>8 <c g> ~
  <c g>8 c4 <e c>8 c4 <c g>
  \repeat volta 2 {
    <c g>4 <c g> s2
    s1
    % Verse 1
    <g e>2 <g e>4. <g e>8 ~
    <g e>8 <g e>4 <c g>8 ~ <c g> <c g>4.
    <c a>4 q8 q8 ~ q8 q4 q8 ~
    q4. q8 q q q q
    <a fs>2 q4. <d a>8 ~
    q q4 <fs d>8 ~ q a,8 a a
    <d b>4 q8 q8 ~ q8 q4 q8 ~
    q4. q4. q4
    c4. c8 ~ c c4 c8 ~
    c8 c4 c8 ~ c c c <e c>
    <e b>4 b8 <gs' e>8 ~ q q4 <e c>8 ~
    <e c>8 ~ <f c>4 <e c>8 ~ <f c>4 s4
    <a, fs>4 q8 <d a>8 ~ q q4 <fs d>8 ~
    q4 q <fs d>4 <fs c>8 q8
    <d b>4 q8 q ~ q q4 <g d>8 ~
    q8 q4 q8 q4 q
    % Chorus
    s1 * 2
    e4 e f8 e d e ~
    e8 f4 g8 f4 e4
    s1 * 5
    s2 <e c>4 s4
    s1 * 8
    e4 e f8 e d e ~
    e8 f4 g8 f4 e4
    s1 * 2
    e4 e f8 e d e ~
    e8 f4 g8 f4 c8 d
  }
  <c g>4 <c g> s2
  s1
  % Last verse
  <g e>2 <g e>4. <g e>8 ~
  <g e>8 <g e>4 <c g>8 ~ <c g> <c g>4.
  <c a>4 q8 q8 ~ q8 q4 q8 ~
  q4. q8 q q q q
  <a fs>2 q4. <d a>8 ~
  q q4 <fs d>8 ~ q a,8 a a
  <d b>4 q8 q8 ~ q8 q4 q8 ~
  q4. q4. q4
  c4. c8 ~ c c4 c8 ~
  c8 c4 c8 ~ c c c <e c>
  <e b>4 b8 <gs' e>8 ~ q q4 <e c>8 ~
  <e c>8 ~ <f c>4 <e c>8 ~ <f c>4 s4
  <a, fs>4 q8 <d a>8 ~ q q4 <fs d>8 ~
  q4 q <fs d>4 <fs c>8 q8
  <d b>4 q8 q ~ q q4 <g d>8 ~
  q8 q4 q8 q4 q
  % Coda
  s1 * 2
  e4 e f8 e d e ~
  e8 f4 g8 f4 e4
  s1 * 5
  s2 e4 s4
  s1 * 8
  e4 e f8 e d e ~
  e8 f4 g8 f4 e4
  s1 * 2
  <c' g>4 q q q8 q ~
  q8 q4 q8 q4 <g e>4
  <c g>4 <c g> s4 r4
  s2. r4
}

leftOne = \relative c {
  \global
  % Intro
  c4 c c c
  c4 c c c
  \repeat volta 2 {
    c c g g8 c8~
    c8 c,4 c8 c4 g'4
    % Verse
    \oneVoice
    c,2 c
    c4. c8 ~ c d e4
    f2 f
    f4 f e ef
    d2 d
    d2 d4 e8 fs
    g2 g2
    g4 g g d8 e
    f4 f f8 a b c ~
    c4 c c c8 d
    e2 e,
    a4 a a b8 c
    d2 d
    d4 d d8 c b a
    g2 g
    g4 g g e
    % Chorus
    f4 f f4. f8 ~
    f8 f4 f8 a4 b
    c8 c c4 c c8 c ~
    c c4 c8 c,4 e
    f2 f4 e8 d8 ~
    d8 d4 d8 f4 fs
    g8 g g4 g4 g8 g8 ~
    g8 g4 g8 g4 e
    f4 f a8 g f4
    c'4 c c8 a g e
    f4 f a f
    c'4 c c e
    f4 f, a f
    c'8 b4 a8 ~ a4 a8 cs
    d4 c b a8 g ~
    g4 g g e
    f4 f f4. f8 ~
    f8 a4 f8 a4 f
    c'8 c c4 c c8 c ~
    c c4 c8 c,4 e
    f4 f f4. f8 ~
    f8 a4 f8 a4 f
    \voiceOne
    c'4 c c c
    c4 c c c
  }
  c c g g8 c8~
  c8 c,4 c8 c4 g'4
  % Last verse
  \oneVoice
  c,2 c
  c4. c8 ~ c d e4
  f2 f
  f4 f e ef
  d2 d
  d2 d4 e8 fs
  g2 g2
  g4 g g d8 e
  f4 f f8 a b c ~
  c4 c c c8 d
  e2 e,
  a4 a a b8 c
  d2 d
  d4 d d8 c b a
  g2 g
  g4 g g e
  % Coda
  \voiceOne
  f4 f f4. f8 ~
  f8 f4 f8 a4 b
  c8 c c4 c c8 c ~
  c c4 c8 c,4 e
  f2 f4 e8 d8 ~
  d8 d4 d8 f4 fs
  g8 g g4 g4 g8 g8 ~
  g8 g4 g8 g4 e
  f4 f a8 g f4
  c'4 c c8 a g e
  f4 f a f
  c'4 c c e
  f4 f, a f
  c'8 b4 a8 ~ a4 a8 cs
  d4 c b a8 g ~
  g4 g g e
  f4 f f4. f8 ~
  f8 a4 f8 a4 f
  c'8 c c4 c c8 c ~
  c c4 c8 c,4 e
  f4 f f4. f8 ~
  f8 a4 f8 a4 g
  c4 c c c
  c4 c c c
  c c g g8 c8~
  c8 <c c,>4 <c c,>8 <c c,>4 r4
}

leftTwo = \relative c, {
  \global
  % Intro
  c2 c
  c2 c
  \repeat volta 2 {
    c2 s2
    s1
    % Verse
    s1 * 16
    % Chorus
    s1 * 22
    c2 c
    c2 c
  }
  c2 s2
  s1
  % Last verse
  s1 * 16
  % Coda
  s1 * 22
  c2 c
  c2 c
  c2 s2
  s2. r4
}

pianoPart = \new PianoStaff \with {
  instrumentName = "Pno."
} <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
  } << { \rightOne } \\ { \rightTwo } >>
  \new Staff = "left" \with {
    midiInstrument = "acoustic grand"
  } { \clef bass << { \leftOne } \\ { \leftTwo } >> }
>>

%%%%

\score {
  <<
    \choirControl
    \transpose c d { \choirPartLayout }
    \transpose c d { \pianoPart }
  >>
  \layout {
    ragged-last = ##t
  }
}

%%%%

verseOneVoice = {
  \verseOneLineOneVoice
  \verseOneAndTwoLineTwoPlusVoice
  \verseOneLineFiveVoice
  \verseOneAndTwoLineSixPlusVoice
}


sopranoVoiceMidi = {
  \global
  R1*4
  % Verse 1
  \verseOneVoice
  \chorusASopranoVoice
  R1*2
  % Verse 2
  R1*16
  \chorusASopranoVoice
  R1*2
  % Last verse
  \verseThreeSopranoVoice
  \chorusBSopranoVoice
}

altoVoiceMidi = {
  \global
  R1*4
  % Verse 1
  \verseOneVoice
  \chorusAAltoVoice
  R1*2
  % Verse 2
  R1*16
  \chorusAAltoVoice
  R1*2
  % Last verse
  \verseThreeAltoVoice
  \chorusBAltoVoice
}

verseTwoVoice = {
  \transpose c' c {
    \verseTwoLineOneVoice
    \verseOneAndTwoLineTwoPlusVoice
    \verseTwoLineFiveVoice
    \verseOneAndTwoLineSixPlusVoice
  }
}

verseThreeTenorVoice = {
  \transpose c' c {
    \verseThreeSopranoVoice
  }
}

chorusBTenorVoice = {
  \transpose c c, {\chorusBSopranoVoice}
}

tenorVoiceMidi = {
  \global
  R1*4
  % Verse 1
  R1*16
  \chorusATenorVoice
  R1*2
  % Verse 2
  \verseTwoVoice
  \chorusATenorVoice
  R1*2
  % Last verse
  \verseThreeTenorVoice
  \chorusBTenorVoice
}

verseThreeBassVoice = {
  \transpose c' c {
    \verseThreeAltoVoice
  }
}

chorusBBassVoice = {
  \transpose c' c {
    \chorusBAltoVoice
  }
}

bassVoiceMidi = {
  \global
  R1*4
  % Verse 1
  R1*16
  \chorusABassVoice
  R1*2
  % Verse 2
  \verseTwoVoice
  \chorusABassVoice
  R1*2
  % Last verse
  \verseThreeBassVoice
  \chorusBBassVoice
}

choirPartMidi = \new ChoirStaff <<
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = "Soprano"
  } <<
    \new Voice = "soprano" { \voiceOne \sopranoVoiceMidi }
  >>
  \new Lyrics \lyricsto "soprano" { \verseOneLyrics \chorusALyrics \chorusALyrics \verseThreeSopranoLyrics \chorusBLyrics  }
  \new Staff \with {
    midiInstrument = "choir aahs"
    instrumentName = "Alto"
  } <<
    \new Voice = "alto" { \voiceTwo \altoVoiceMidi }
  >>
  \new Lyrics \lyricsto "alto" { \verseOneLyrics \chorusALyrics \chorusALyrics \verseThreeBassLyrics \chorusBLyrics }
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef "treble_8"
    \new Voice = "tenor" { \voiceOne \tenorVoiceMidi }
  >>
  \new Lyrics \lyricsto "tenor" { \chorusALyrics \verseTwoLyrics \chorusALyrics \verseThreeSopranoLyrics \chorusBLyrics }
  \new Staff \with {
    midiInstrument = "choir aahs"
  } <<
    \clef bass
    \new Voice = "bass" { \voiceTwo \bassVoiceMidi }
  >>
  \new Lyrics \lyricsto "bass" { \chorusALyrics \verseTwoLyrics \chorusALyrics \verseThreeBassLyrics \chorusBLyrics }
>>

\score { \unfoldRepeats \articulate
  <<
    \transpose c d  { \choirPartMidi }
    \transpose c d  { \pianoPart }
  >>
  \midi { }
}

rehearsalMidi = #
(define-music-function
 (parser location name midiInstrument lyrics) (string? string? ly:music?)
 #{
   \unfoldRepeats <<
     \new Staff = "soprano" \new Voice = "soprano" { \transpose c d { \sopranoVoiceMidi } }
     \new Staff = "alto" \new Voice = "alto" { \transpose c d { \altoVoiceMidi } }
     \new Staff = "tenor" \new Voice = "tenor" { \transpose c d { \tenorVoiceMidi } }
     \new Staff = "bass" \new Voice = "bass" { \transpose c d { \bassVoiceMidi } }
     \context Staff = $name {
       \set Score.midiMinimumVolume = #0.5
       \set Score.midiMaximumVolume = #0.5
       \set Score.midiInstrument = "clav"
       \set Staff.midiMinimumVolume = #0.8
       \set Staff.midiMaximumVolume = #1.0
       \set Staff.midiInstrument = $midiInstrument
     }
     \new Lyrics = $(string-append name "-lyrics") \with {
       alignBelowContext = $name
     } \lyricsto $name $lyrics
     \transpose c d { \pianoPart }
     \context Staff = "right" {
       \set Staff.midiMinimumVolume = #0.3
       \set Staff.midiMaximumVolume = #0.3
       \set Staff.midiInstrument = "synthbrass 1"
     }
     \context Staff = "left" {
       \set Staff.midiMinimumVolume = #0.3
       \set Staff.midiMaximumVolume = #0.3
       \set Staff.midiInstrument = "synthbrass 1"
     }
   >>
 #})

\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" { \verseOneLyrics \chorusALyrics \chorusALyrics \verseThreeSopranoLyrics \chorusBLyrics  }
    \midi { }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" { \verseOneLyrics \chorusALyrics \chorusALyrics \verseThreeBassLyrics \chorusBLyrics }
    \midi { }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" { \chorusALyrics \verseTwoLyrics \chorusALyrics \verseThreeSopranoLyrics \chorusBLyrics }
    \midi { }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" { \chorusALyrics \verseTwoLyrics \chorusALyrics \verseThreeBassLyrics \chorusBLyrics }
    \midi { }
  }
}

% Unused

altoHarmonyI = \relative c' {
 \global
 \dynamicUp
 R1*4
 % Verse
 \slurDashed c8( c) \slurSolid c4 c c8 e ~
 e8 c ~ c2 r4
 f4 f8 a ~ a g4 f8 ~
 f2. r8 e
 d4 d8 d ~ d e4 fs8 ~
 fs8 g4 a8 ~ a4 r8 d,
 g4 g8 b ~ b a4 g8 ~
 g2 r4 g4
 f4 e8 d ~ d f4 d8 ~
 d8 d4 \slurDashed c8( c4) \slurSolid r8 b8
 e4 e8 gs ~ gs gs4 a8 ~
 a2 r4. e8
 d4 e8 fs ~ fs g4 a8( ~
 a8 b) a2 r4
 g4 g8 g ~ g a4 b8 ~
 b2. r4
 % Chorus
}

altoHarmonyII = \relative c' {
  % 2nd time
  s1 * 16
  c,8 c c4 c c8 c8 ~
  c8 c4 c8 ef4 d
  c1 ~
  c2 r2
  c8 c c4 c c8 d8 ~
  d8 d4 d8 d4 d
  b1 ~
  b2 r4 e8 d
  c4. c8 f g f e ~
  e8 e ~ e4 r8 e e d
  c8 c c d f4 f8 f( ~
  f4. e8 ~ e4) r8 e
  f8( e) d4 d8( e8) f8 e8 ~
  e8 e4 e8 ~ e4 r8 e8
  d4 d4 d8( e) fs g ~
  g2. r4
  f8 f f4 f f8 f8 ~
  f8 f4 f8 f4 f
  e1 ~
  e2 r2
  f8 f f4 f f8 f8 ~
  f8 f4 f8 a4 a
  g1 ~
  g2 r2
  R1
  R1
}

tenorHarmonyI = \relative c {
 \global
 \dynamicUp
 R1*4
 % Verse
 R1
 r4. c8 ~ c d e4
 f2. r4
 r8 \slurDashed f8( f) \slurSolid f e e ef ef
 d4. d8 d4 d8 d ~
 d8 d4 d8 ~ d4 e8( fs)
 g4. g8 g8 a4 b8 ~
 b8 c4 d8 ~ d2
}

tenorHarmonyII = \relative c {
  % 2nd time
  s1 * 16
  a,8 a a4 a a8 a8 ~
  a8 a4 a8 a4 a
  g1 ~
  g2 r2
  a8 a a4 a a8 a8 ~
  a8 a4 a8 a4 a
  d1 ~
  d2 r4 b8 b
  c4. a8 a a b c ~
  c8 bf ~ bf4 r8 bf8 bf b
  c8 f, a b c8( a) b8 c8 ~
  c2. r8 b8
  a4 a f c'8 c8~
  c d4 c8 ~ c4 r8 a8
  fs4 a a8( b) c b( ~
  b8 c4 d8 c4) r4
  a8 a a4 a f8 a8 ~
  a8 a4 f8 a4 c
  g1 ~
  g2 r2
  a8 a a4 a f8 a8 ~
  a8 a4 f8 a4 c
  e1 ~
  e2 r2
  R1
  R1
}

bassHarmony = \relative c {
 \global
 \dynamicUp
 R1*4
 % Verse
 R1
 r4. c8 ~ c d e4
 f2. r4
 r8 \slurDashed f8( f) \slurSolid f e e ef ef
 d4. d8 d4 d8 d ~
 d8 d4 d8 ~ d4 b8( a)
 g4. g8 g8 a4 b8 ~
 b8 c4 d8 ~ d2
}
