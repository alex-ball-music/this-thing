\version "2.18.2"

\include "english.ly"
\include "ajb-setup.ly"
theStaffSize = #18
\include "ajb-paper.ly"
\include "ajb-layout.ly"

\header {
 dedication = "For Viv Wilkinson"
 title = "This Thing That is Come to Pass"
 composer = "Alex Ball"
 copyright = "© Alex Ball 2000, released under a Creative Commons Attribution-NonCommercial 3.0 Licence"
 % Remove default LilyPond tagline
 tagline = ##f
}

\paper {
 #(set-paper-size "a4")
}

global = {
 \tempo 4=160
 \key c \major
 \time 2/2
 \revert MultiMeasureRest.staff-position
}

soprano = \relative c' {
 \global
 % Chrous
 f8 f f4 f f8 f8 ~
 f8 f4 f8 f4 f
 e1 ~
 e2 r2
 f8 f f4 f f8 f8 ~
 f8 f4 f8 f4 a
 g1 ~
 g2 r4 g8 g8
 f4 f8 f ~ f f f f ~
 f8 f ~ f2 r8 f8
 e8 e4 e8 f( e) d e~
 e2 r4 c4
 f4 f8 f ~ f f4 a8 ~
 a8 g4 f8 ~ f4 r8 f8
 g4 g8 g ~ g a4 b8 ~
 b2. r4
 c8 c c4 c a8 c8 ~
 c8 c4 a8 c4 a
 c1 ~
 c2 r2
 c8 c c4 c c8 c8 ~
 c8 c4 c8 ef4 d
 c1 ~
 c2 r2
 % Verse
 c,8 c c4 c c8 e ~
 e8 c ~ c2 r4
 f4 f8 a ~ a g4 f8 ~
 f2. r8 e
 d4 d8 d ~ d e4 fs8 ~
 fs8 g4 a8 ~ a4 r8 d,
 g4 g8 b ~ b a4 g8 ~
 g2 r4 g4 
 a4 g8 f ~ f a4 g8 ~
 g8 f4 e8 ~ e4 r8 g8
 gs4 e8 b' ~ b c4 a8 ~
 a2 r4. e8
 d4 e8 fs ~ fs g4 a8( ~
 a8 b) a2 r4
 g4 g8 g ~ g a4 b8 ~
 b2. r4
}

alto = \relative c' {
 \global
 % Music follows here.
 c8 c c4 c c8 c8 ~
 c8 c4 c8 ef4 d
 c1 ~
 c2 r2
 c8 c c4 c c8 d8 ~
 d8 d4 d8 d4 d
 b1 ~
 b2 r4 b8 b
 c4 c8 a ~ a a b c ~
 c8 c ~ c2 r8 c8
 c8 c4 c8 c4 b8 c ~
 c2 r4 c4
 c4 c8 c ~ c c4 f8 ~
 f8 e4 d8 ~ d4 r8 c8
 b4 b8 b ~ b d4 g8 ~
 g2. r4
 f8 f f4 f f8 f8 ~
 f8 f4 f8 f4 f
 e1 ~
 e2 r2
 f8 f f4 f f8 f8 ~
 f8 f4 f8 f4 f
 e1 ~
 e2 r2
}

tenor = \relative c' {
 \global
 % Music follows here.
 a8 a a4 a a8 a8 ~
 a8 a4 a8 a4 a
 g1 ~
 g2 r2
 a8 a a4 a a8 a8 ~
 a8 a4 a8 a4 a
 d1 ~
 d2 r4 g,8 g
 a4 a8 f ~ f f g a ~
 a8 a ~ a2 r8 a8
 g8 g4 g8 g4 g8 g~
 g2 r4 g4
 a4 a8 a ~ a a4 f8 ~
 f8 g4 a8 ~ a4 r8 a8
 d4 d8 d ~ d b4 d8 ~
 d2. r4
 a8 a a4 a f8 a8 ~
 a8 a4 f8 a4 c
 g1 ~
 g2 r2
 a8 a a4 a f8 a8 ~
 a8 a4 f8 a4 c
 g1 ~
 g2 r2
}

bass = \relative c, {
 \global
 % Music follows here.
 f8 f f4 f f8 f8 ~
 f8 f4 f8 a4 b
 c1 ~
 c2 r2
 f8 f f4 f e8 d8 ~
 d8 d4 d8 f4 fs
 g2( ~ g8 d4 g,8) ~
 g2 r4 e8 e
 f4 f8 a ~ a a g f ~
 f8 f ~ f2 r8 f8
 c'8 c4 c8 ef4 d8 c~
 c2 r4 e4
 f4 f8 f ~ f e4 d8 ~
 d8 d4 d8 ~ d4 r8 d8
 g4 g8 g ~ g d4 g,8 ~
 g2. r4
 f8 f f4 f f8 f8 ~
 f8 a4 f8 a4 f
 c'1 ~
 c2 r2
 f,8 f f4 f f8 f8 ~
 f8 a4 f8 a4 f
 c'1 ~
 c2 r2
}

verseOne = \lyricmode {
 \set stanza = "1."
 % Chorus
 Let us go see this thing that is come to pass! __
 Let us go see this thing that is come to pass! __
 As the Lord a -- bove is our wit -- ness,
 We’ll wit -- ness our Lord on earth.
 We’ll praise him till the end of time,
 Let’s praise him at his birth!
 Let us go see this thing that is come to pass! __
 Let us go see this thing that is come to pass! __
 
 % Verse 1
 Shep -- herds in fields a -- bi -- ding,
 Keep -- ing watch by night,
 The an -- gel of the Lord ap -- peared,
 The dark -- ness turned to light.
 ‘To -- day in Da -- vid’s town,’ he said,
 ‘The Sa -- viour’s life be -- gan,’
 The shep -- herds start -- ed run -- ning,
 Shout -- ing as they ran:
 
}

verseTwo = \lyricmode {
 \set stanza = "2."
 % Lyrics follow here.
%  East -- ern ma -- gi, tra -- cing
%  Move -- ment in the skies,
%  Dis -- co -- vered signs a -- mong the stars,
%  That bright -- ened up their eyes.
%  They knew they had to fol -- low the star,
%  They worked out what it meant,
%  They hur -- ried off to Ju -- dah,
%  sing -- ing as they went:
}

verseThree = \lyricmode {
 \set stanza = "3."
 % Lyrics follow here.
%  Here we are at Christ -- mas,
%  Do -- ing what we can
%  To ce -- le -- brate that bles -- sed time
%  When God be -- came a man.
%  We may have missed things first time round
%  But in our hearts we're there:
%  We’ve seen the signs and won -- ders,
%  Let’s show that we care:
}

right = \relative c' {
 \global
 % Music follows here.
 <f c a>2 <f c a>4. <f c a>8 ~
 <f c a>8 <f c a>4 <f c a>8 <f ef c a>4 <f d a>
 <e c g>8 <e c g> <e c g>4 <f c> <d c g>8 <e c g> ~
 <e c g>8 <f c>4 <g e c>8 <f c>4 <e c g>4
 <f c a>2 <f c a>4. <f d a>8 ~
 <f d a>8 <f d a>4 <f d a>8 <f d a>4 <a fs d c>
 <g d b>8 <g d b>8 <g d b>4 <g d b>4 <g d b>8 <b g d>8 ~
 <b g d>8 <a g d>4 <g d b>8 <g d b>2
 <f c a>2 <f c a>4. <f c a>8 ~
 <f c a>8 <f c a>4 <f c a>8 <f ef c a>4 <f d a>
 <e c g>8 <e c g>4 <e c g>8 <f c> <e c> <d c g> <e c g> ~
 <e c g>8 <f c>4 <g e c>8 <f c>4 <e c g>4
 <f c a>2 <f c a>4. <a f d>8 ~
 <a f d>8 <g d>4 <f d a>8 ~ <f d a>4 <f d c>
 <g d b>4 <g d b>8 <g d b> ~ <g d b> <a d,>4 <b g d>8 ~
 <b g d>8 <a g d>4 <g d b>8 <g d b>2
}

left = \relative c, {
 \global
 % Music follows here.
 f2 f4. f8 ~
 f8 f4 f8 a4 b
 c8 c c4 c c8 c ~
 c c4 c8 c,4 e
 f2 f4 e8 d8 ~
 d8 d4 d8 f4 fs
 g8 g g4 g4 g8 g8 ~
 g8 g4 g8 g4 g
 f2 f4. f8 ~
 f8 f4 f8 a4 b
 c4 c c c
 c4 c, d e
 f2 f4 e8 d8 ~
 d8 d4 d8 d4 f
 g8 g g4 g4 g8 g8 ~
 g8 g4 g8 g4 g
}

choirPart = \new ChoirStaff <<
 \new Staff \with {
  midiInstrument = "choir aahs"
  instrumentName = \markup \center-column { "S." "A." }
 } <<
  \new Voice = "soprano" { \voiceOne \soprano }
  \new Voice = "alto" { \voiceTwo \alto }
 >>
 \new Lyrics \lyricsto "soprano" \verseOne
 \new Lyrics \lyricsto "soprano" \verseTwo
 \new Lyrics \lyricsto "soprano" \verseThree
 \new Staff \with {
  midiInstrument = "choir aahs"
  instrumentName = \markup \center-column { "T." "B." }
 } <<
  \clef bass
  \new Voice = "tenor" { \voiceOne \tenor }
  \new Voice = "bass" { \voiceTwo \bass }
 >>
>>

pianoPart = \new PianoStaff \with {
 instrumentName = "Pno."
} <<
 \new Staff = "right" \with {
  midiInstrument = "acoustic grand"
 } \right
 \new Staff = "left" \with {
  midiInstrument = "acoustic grand"
 } { \clef bass \left }
>>

\score {
 <<
  \choirPart
  \pianoPart
 >>
 \layout { }
 \midi { }
}
